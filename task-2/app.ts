const chessBoard = function(size:number){
    let black:string = '';
    const white:string = '  ';
    for (let i:number = 0; i < size; i++) {
        black += '\n'
        for (let j:number = 0; j < size; j++) {
            if ((i + j) % 2 == 0) {
                black += white
            } else {
                black += "██"
            }
        }
    }
    console.log(black)
}
chessBoard(8)